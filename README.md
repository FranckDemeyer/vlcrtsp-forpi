[![Docker Pulls](https://img.shields.io/docker/pulls/fdemeyer/vlcrtsp-forpi.svg)](https://hub.docker.com/r/fdemeyer/vlcrtsp-forpi)

vlcrtsp-forpi
=================
a simple docker container using vlc to stream cam in rtsp with some configurable options

Using Docker image
================

* get the help :

	docker run -it fdemeyer/vlcrtsp-forpi -h

* run the container specifying parameters

	docker run -p 8554:8554 --name="vlcrtsp-forpi" --hostname="vlcrtsp-forpi" --device=/dev/video0 (--device=/dev/snd) fdemeyer/vlcrtsp-forpi -a
