FROM alpine:latest

RUN adduser --disabled-password pi
RUN adduser pi video
RUN adduser pi audio
RUN apk add --no-cache vlc

COPY resources/entrypoint.sh /
RUN chmod +x /entrypoint.sh
USER pi
EXPOSE 8554
ENTRYPOINT ["sh", "/entrypoint.sh"]
